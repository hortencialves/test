

# Guide

This project is a cypress test for QA Analyst.

Two specs were developed. In the first spec created a new presentation based on the Catalog type. The second spec is a search for presentations that contain the word 'bot'.

In some parts of the implementation the XPath plugin was used and the locators are located in a support folder in a locators.ts file.
  
# Bugs found Part1

  

- When creating a new presentation “Presentation based on” there is no warning which is mandatory. An asterisk or text saying it is required.

  

  

- When choosing Catalog, in Products And Assets Catalog by name, in the Search Catalog field you must indicate that it is mandatory before selecting the “Create Presentation” option.

  

  

- The optional field “Search Custom” is not selectable.

  

  

- Choosing presentation based on “Customer” does not indicate that the field is mandatory before selecting the “Create Presentation” option.

  

  

- The search by customer doesn't work, but by location. The field should change to location.

  

  

- In the “Search Custom Catalog” field there is an option “None” which is not selectable.

  

  

- The “Select Season” field does not indicate that it is mandatory with a text or an asterisk before selecting the “Create Presentation” option.



  

In some parts of the implementation the XPath plugin was used and the locators are located in a support folder in a locators.ts file.

# Bugs found Part2

  

- When searching for “All” using the “Presentations” filter, the information “All” is not saved in the “Presentations” field as it happens when it is done by “Mine” as a Placeholder.

- When searching for “All” using the “Division” filter, the information “All” is not saved in the “Division” field as when searching for “Division 5588”/“Division 6554”/“Division2979” as Placeholder.

- When searching for “All” using the “Season” filter, the information “All” is not saved in the “Season” field as Placeholder.
