const locators = {
    DASHBOARD: {
        XP_FRONT_COVER: "//p[normalize-space()='Showroom']",
        ENTER: '.AppSelectionsc__StyledAppSelectionList-sc-crqgqq-1',
    
    },
    THE_DIGITAL_SHOWROOM: {
        CREATE_PRESENTATION:'[data-testid="icon-Plus"]',
        XP_PRESENTATION_FILTER: "//p[normalize-space()='Presentations']",
        XP_PRESENTATION_FILTER_ALL: "//button[@class='ClickableItemsc__StyledClickableItem-sc-jvfv9r-0 ggDZot'][normalize-space()='All']",
        XP_TYPE_FILTER: "//button[@class='AnimatedInputsc__StyledIconButton-sc-1jrrq74-2 cuXjWi']",
        FN_XP_SEARCH_RESULT: search => `//div[contains(text(),'${search}')]`
    },
    PRESENTENTATION_FORM:{
        NAME: '#presentationName',
        OPEN_PRESENTATION_IMEDIATILLY: '.CheckboxInputsc__StyledCheckboxLabel-sc-1uuczkl-1',
        CATALOG_BASED_ON: ':nth-child(2) > .FieldLabelsc__StyledFieldLabel-sc-1be16qd-0',
        SEARCH_CATALOG_BY_NAME: '.PresentationFormCatalogSelectorsc__StyledBrandCatalogDropdown-sc-1ujuzvp-2 > [data-testid="dropdown-menu"] > .DropdownMenusc__StyledDropdownMenuContent-sc-1gvbwbj-0 > :nth-child(1) > .FieldInputTextsc__StyledFieldInputText-sc-b5r1ur-0',
        THIRD_CATALOG_SELECTED: "div[class='PresentationFormCatalogSelectorsc__StyledBrandCatalogDropdown-sc-1ujuzvp-2 kfMcP'] button:nth-child(3)",
        SELECT_DIVISION: '.RjSsp > .DropdownMenusc__StyledDropdownMenuContent-sc-1gvbwbj-0 > .DropdownTriggersc__StyledDropdownTrigger-sc-ih66vr-0',
        FIRST_DIVISION_SELECTED: '[style="will-change: transform; transform: translateY(0px); max-height: calc(26rem);"] > [data-testid="dropdown-list"] > :nth-child(1)',
        SELECT_SEASON:':nth-child(6) > .DropdownMenusc__StyledDropdownMenuContent-sc-1gvbwbj-0 > .DropdownTriggersc__StyledDropdownTrigger-sc-ih66vr-0',
        FIRST_SEASON_SELECTED: '[style="will-change: transform; transform: translateY(0px); max-height: calc(26rem);"] > [data-testid="dropdown-list"] > :nth-child(1)',
        FIRST_LOCATION_SELECTED: '[data-testid="customerSearchBoxList"] > :nth-child(1) > :nth-child(1)',
        BTN_SUBMIT_CREATE_PRESENTATION: ".ClickableItemsc__StyledClickableItem-sc-jvfv9r-0.daOBPy",
        NOTIFICATION_MESSAGE: ".Notificationsc__StyledNotificationContent-sc-1my9le0-1 > p"
    }
   
}
export default locators;