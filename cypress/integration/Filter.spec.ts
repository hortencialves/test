/// <reference types="cypress-xpath" />
import loc from '../support/locators'
describe('Start here', () => {
  beforeEach(() => {
    cy.login(Cypress.env('AUTH_USERNAME'), Cypress.env('AUTH_PASSWORD'));
  });
  
  const search = ('Bot')
  it('Filter All Bot presentation', () => {
    cy.visit('?skip_browser_check=true')
    cy.get(loc.DASHBOARD.ENTER).click()
    cy.xpath(loc.THE_DIGITAL_SHOWROOM.XP_PRESENTATION_FILTER).click()
    cy.xpath(loc.THE_DIGITAL_SHOWROOM.XP_PRESENTATION_FILTER_ALL).click()
    cy.xpath(loc.THE_DIGITAL_SHOWROOM.XP_TYPE_FILTER).type(search,{ timeout: 1000 })
    cy.xpath(loc.THE_DIGITAL_SHOWROOM.FN_XP_SEARCH_RESULT(search))
      .should('have.contain', `${search}`)
  })
});



