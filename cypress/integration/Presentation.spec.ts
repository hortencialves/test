/// <reference types="cypress-xpath" />
import loc from '../support/locators'
describe('Start here', () => {
  beforeEach(() => {
    cy.login(Cypress.env('AUTH_USERNAME'), Cypress.env('AUTH_PASSWORD'));
  });
  
  
  const time = new Date().getTime();

  it('Create New Presentation with sucess - Catalog', () => {
    cy.visit('?skip_browser_check=true');
    cy.wait(1000)
    cy.xpath(loc.DASHBOARD.XP_FRONT_COVER).should('have.text', 'Showroom')
    cy.get(loc.DASHBOARD.ENTER).click();
    cy.get(loc.THE_DIGITAL_SHOWROOM.CREATE_PRESENTATION).click();
    cy.get(loc.PRESENTENTATION_FORM.NAME).type(`presentation-example${time}`)
    cy.get(loc.PRESENTENTATION_FORM.OPEN_PRESENTATION_IMEDIATILLY).click()
    cy.get(loc.PRESENTENTATION_FORM.CATALOG_BASED_ON).click()
    cy.get(loc.PRESENTENTATION_FORM.SEARCH_CATALOG_BY_NAME).click()
    cy.get(loc.PRESENTENTATION_FORM.THIRD_CATALOG_SELECTED).click()
    cy.get(loc.PRESENTENTATION_FORM.SELECT_DIVISION).click()
    cy.get(loc.PRESENTENTATION_FORM.FIRST_DIVISION_SELECTED).click()
    cy.get(loc.PRESENTENTATION_FORM.SELECT_SEASON).click()
    cy.get(loc.PRESENTENTATION_FORM.FIRST_DIVISION_SELECTED).click()
    cy.get(loc.PRESENTENTATION_FORM.FIRST_LOCATION_SELECTED).click()
    cy.get(loc.PRESENTENTATION_FORM.BTN_SUBMIT_CREATE_PRESENTATION).click()
    cy.get(loc.PRESENTENTATION_FORM.NOTIFICATION_MESSAGE)      
      .should('have.contain', `presentation-example${time}`)
  });

  
});

